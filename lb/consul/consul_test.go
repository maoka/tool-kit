package consul

import (
	"encoding/json"
	"testing"
)

func TestInit(t *testing.T) {
	Init("http://127.0.0.1:8500")
	err := Register("test", "007", "", 6378, "hubilie")
	if err != nil {
		t.Fatal(err)
	}
	svr, err := Services("test", "hubilie", false)
	if err != nil {
		t.Fatal(err)
	}
	ab, err := json.Marshal(svr)
	t.Log(string(ab))
	err = UnRegister("test", "007")
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetKeyValue(t *testing.T) {
	Init("http://127.0.0.1:8500")
	err := StoreKeyValue("test", []byte(`{"data":"xiexie"}`))
	if err != nil {
		t.Fatal(err)
	}
	ab, err := GetKeyValue("test")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(ab))
	err = DelKeyValue("test")
	if err != nil {
		t.Fatal(err)
	}
}
