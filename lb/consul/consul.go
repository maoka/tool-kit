package consul

import (
	"fmt"
	"net/url"
	"os"

	stdconsul "github.com/hashicorp/consul/api"
)

var consulConfig *stdconsul.Config

type SvrNode struct {
	ID      string
	Service string
	Address string
	Port    int
	Tags    []string
}

//初始化连接
func Init(addr string) error {
	urlObj, err := url.Parse(addr)
	if err != nil {
		return fmt.Errorf("urlparse failed. url(%s) %v", addr, err)
	}
	consulConfig = stdconsul.DefaultConfig()
	var cAddr string
	if urlObj.Port() == "" {
		cAddr = urlObj.Hostname()
	} else {
		cAddr = fmt.Sprintf("%s:%s", urlObj.Hostname(), urlObj.Port())
	}
	consulConfig.Address = cAddr
	consulConfig.Scheme = urlObj.Scheme
	return nil
}

//注册服务
//id为空会使用当前服务host
func Register(appName string, id string, host string, port int, tags ...string) error {
	client, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		return fmt.Errorf("consul: consul.NewClient failed %v", err)
	}
	if host == "" {
		host, err = os.Hostname()
		if err != nil {
			return fmt.Errorf("consul: os.Hostname failed %v", err)
		}
	}
	if id == "" {
		id = host
	}

	var tag = []string{"metrics", id}
	for _, v := range tags {
		tag = append(tag, v)
	}

	var registration = &stdconsul.AgentServiceRegistration{
		ID:      fmt.Sprintf("%s.%s", appName, id), //当前实例ID 容器启动可以直接用容器host
		Name:    appName,                           //服务名称​同一个服务用同一个
		Tags:    tag,
		Port:    port, //端口​
		Address: host, //地址
		Check: &stdconsul.AgentServiceCheck{ //健康检查​
			TCP:                            fmt.Sprintf("%s:%d", host, port), //健康检查地址
			Timeout:                        "3s",                             //超时时间​​
			Interval:                       "5s",                             //多久检查一次​​
			DeregisterCriticalServiceAfter: "15s",                            //15秒没有收到返回数据就在发现服务摘除服务​
		},
	}
	err = client.Agent().ServiceRegister(registration)
	if err != nil {
		return fmt.Errorf("consul: client.Agent().ServiceRegister failed. %v", err)
	}
	return nil
}

//注销
func UnRegister(appName string, id string) error {
	client, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		return fmt.Errorf("consul: consul.NewClient failed %v", err)
	}
	if id == "" {
		id, err = os.Hostname()
		if err != nil {
			return fmt.Errorf("consul: os.Hostname failed %v", err)
		}
	}
	err = client.Agent().ServiceDeregister(fmt.Sprintf("%s_%s", appName, id))
	if err != nil {
		return fmt.Errorf("consul: client.Agent().ServiceDeregister id %s_%s failed %v", appName, id, err)
	}
	return nil
}

//获取服务列表
func Services(appName string, tag string, healthyOnly bool) ([]SvrNode, error) {
	client, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		return nil, fmt.Errorf("consul: consul.NewClient failed %v", err)
	}
	svr, _, err := client.Health().Service(appName, tag, healthyOnly, nil)
	var svrArr []SvrNode
	for _, v := range svr {
		svrArr = append(svrArr, SvrNode{
			ID:      v.Service.ID,
			Service: v.Service.Service,
			Address: v.Service.Address,
			Port:    v.Service.Port,
			Tags:    v.Service.Tags,
		})
	}
	return svrArr, nil
}

func StoreKeyValue(key string, value []byte) error {
	client, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		return fmt.Errorf("consul: consul.NewClient failed %v", err)
	}
	kv := &stdconsul.KVPair{
		Key:   key,
		Flags: 0,
		Value: value,
	}
	_, err = client.KV().Put(kv, nil)
	if err != nil {
		return fmt.Errorf("consul: client.KV().Put failed %v", err)
	}
	return nil
}

func GetKeyValue(key string) ([]byte, error) {
	client, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		return nil, fmt.Errorf("consul: consul.NewClient failed %v", err)
	}
	kv, _, err := client.KV().Get(key, nil)
	if err != nil {
		return nil, fmt.Errorf("consul: client.KV().Get failed %v", err)
	}
	return kv.Value, nil
}

func DelKeyValue(key string) error {
	client, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		return fmt.Errorf("consul: consul.NewClient failed %v", err)
	}
	_, err = client.KV().Delete(key, nil)
	if err != nil {
		return fmt.Errorf("consul: client.KV().Delete failed %v", err)
	}
	return nil
}
