package lb

type Instance interface {
	GetId() string
	Call(method string, request interface{}, response interface{}) error
}

type Instancer interface {
	GetInstances() []Instance
	GetInstance(id string) Instance
}

type Balancer interface {
	Next() Instance
}
