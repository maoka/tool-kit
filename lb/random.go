package lb

import (
	"math/rand"
)

func NewRandom(s Instancer, seed int64) Balancer {
	return &random{
		s: s,
		r: rand.New(rand.NewSource(seed)),
	}
}

type random struct {
	s Instancer
	r *rand.Rand
}

func (r *random) Next() Instance {
	endpoints := r.s.GetInstances()
	if endpoints == nil {
		return nil
	}
	if len(endpoints) <= 0 {
		return nil
	}
	return endpoints[r.r.Intn(len(endpoints))]
}
