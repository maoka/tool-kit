package peer

type Acceptor interface {
	Start(string, *SessionManager) error
}

var acceptorList = make(map[string]Acceptor)

func RegisterAcceptor(name string, server Acceptor) {
	if server == nil {
		panic("acceptor: Register provide is nil")
	}
	if _, dup := acceptorList[name]; dup {
		panic("acceptor: Register called twice for provide " + name)
	}
	acceptorList[name] = server
}

func GetAcceptor(name string) Acceptor {
	if v, ok := acceptorList[name]; ok {
		return v
	}
	return nil
}
