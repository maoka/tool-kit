package websocket

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/gorilla/websocket"
	"gitlab.com/maoka/tool-kit/peer"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type wsAcceptor struct{}

func (ws *wsAcceptor) Start(addr string, sessMgr *peer.SessionManager) error {
	urlObj, err := url.Parse(addr)
	if err != nil {
		return fmt.Errorf("websocket urlparse failed. url(%s) %v", addr, err)
	}
	if urlObj.Path == "" {
		return fmt.Errorf("websocket start failed. expect path in url to listen addr:%s", addr)
	}
	http.HandleFunc(urlObj.Path, func(w http.ResponseWriter, r *http.Request) {
		c, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			fmt.Println(err)
			return
		}
		sessMgr.Register <- peer.NewSession(newConnection(c, sessMgr))
	})
	err = http.ListenAndServe(urlObj.Host, nil)
	if err != nil {
		return fmt.Errorf("websocket ListenAndServe addr:%s failed:%v", addr, err)
	}
	return nil
}

func init() {
	peer.RegisterAcceptor("ws", new(wsAcceptor))
}
