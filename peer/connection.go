package peer

type Connection interface {
	// 获得原始连接
	Raw() interface{}
	// 获得管理
	Peer() *SessionManager
	// 发送消息
	//Send(msg interface{})
	Send(msg []byte) error
	// 断开
	Close()
	// 标示ID
	ID() int64
	// 访问地址
	RemoteAddr() string

	IsClosed() bool
}

type ConnectionIdentify struct {
	id int64
}

func (ci *ConnectionIdentify) ID() int64 {
	return ci.id
}

func (ci *ConnectionIdentify) SetID(id int64) {
	ci.id = id
}
