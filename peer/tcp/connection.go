package tcp

//
//import (
//	"gitlab.com/maoka/tool-kit/peer"
//	"gitlab.com/maoka/tool-kit/utils"
//	"net"
//	"sync"
//)
//
//type tcpConnection struct {
//	peer.ConnectionIdentify
//
//	p *peer.SessionManager
//
//	conn net.Conn
//
//	// 退出同步器
//	exitSync sync.WaitGroup
//
//	// 发送队列
//	sendQueue *utils.Pipe
//
//	cleanupGuard sync.Mutex
//}
//
//// 获得原始连接
//func (tc *tcpConnection) Raw() interface{} {
//	return tc.conn
//}
//
//// 获得管理
//func (tc *tcpConnection) Peer() *peer.SessionManager {
//	return nil
//}
//
//// 发送消息
////Send(msg interface{})
//func (tc *tcpConnection) Send(msg []byte) {
//
//}
//
//// 断开
//func (tc *tcpConnection) Close() {
//
//}
//
//// 标示ID
//func (tc *tcpConnection) ID() int64 {
//	return tc.ConnectionIdentify.ID()
//}
//
//// 访问地址
//func (tc *tcpConnection) RemoteAddr() string {
//	//return tc.conn
//	return tc.conn.RemoteAddr().String()
//}
//
//// 判断连接是否断开
//func (tc *tcpConnection) IsClosed() bool {
//	return false
//}
//
//// 接收循环 TODO: 拆包粘包处理
//func (tc *tcpConnection) recvLoop() {
//	for tc.conn != nil {
//		var data []byte
//		_, err := tc.conn.Read(data)
//		if err != nil {
//			break
//		}
//		tc.p.ProcessMessage(tc.ID(), data)
//	}
//	tc.Close()
//	tc.cleanup()
//}
//
//func (tc *tcpConnection) sendLoop() {
//	var writeList []interface{}
//	for {
//		writeList = writeList[0:0]
//		exit := tc.sendQueue.Pick(&writeList)
//		if exit {
//			break
//		}
//		for _, msg := range writeList {
//			tc.conn.Write(msg.([]byte))
//		}
//	}
//	tc.cleanup()
//}
//
//func (tc *tcpConnection) cleanup() {
//
//	tc.cleanupGuard.Lock()
//	defer tc.cleanupGuard.Unlock()
//
//	if tc.conn != nil {
//		tc.conn.Close()
//		tc.conn = nil
//	}
//	tc.exitSync.Done()
//}
//
//func (tc *tcpConnection) init() {
//	tc.exitSync.Add(2)
//	go tc.recvLoop()
//	go tc.sendLoop()
//	go func() {
//		tc.exitSync.Wait()
//		tc.p.Close(tc.ID())
//	}()
//}
//
//func newConnection(conn net.Conn,
//	p *peer.SessionManager) *tcpConnection {
//	wsc := &tcpConnection{
//		conn:      conn,
//		sendQueue: utils.NewPipe(),
//		p:         p,
//	}
//	wsc.init()
//	return wsc
//}
