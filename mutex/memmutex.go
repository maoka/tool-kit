package mutex

import (
	"encoding/base64"
	"sync"
	"time"
)

type TimeOutMutex struct {
	isLocked   bool
	lock       sync.Mutex
	lockTime   time.Time
	expireTime time.Duration
}

func (l *TimeOutMutex) Lock() {
	if l.isLocked {
		if time.Now().Sub(l.lockTime) >= l.expireTime+2*time.Millisecond {
			l.Unlock()
		}
	}
	l.lock.Lock()
	l.lockTime = time.Now()
	l.isLocked = true
}
func (l *TimeOutMutex) Unlock() {
	if l.isLocked {
		l.lock.Unlock()
		l.isLocked = false
	}
}
func (l *TimeOutMutex) CheckTimeOut(timedel time.Duration) bool {
	if time.Now().Sub(l.lockTime) > timedel+2*time.Millisecond {
		return true
	}
	return false
}

type MemObjectMutex struct {
	lockMap    map[interface{}]sync.Locker
	lock       sync.Mutex
	expireTime time.Duration
	gcTime     time.Duration
	prefix     string
}

func (mol *MemObjectMutex) Lock(key string) {
	realKey := base64.StdEncoding.EncodeToString([]byte(mol.prefix + key))
	mol.lock.Lock()
	lock, ok := mol.lockMap[realKey]
	if !ok {
		lock = &TimeOutMutex{
			expireTime: mol.expireTime,
			isLocked:   false,
		}
		mol.lockMap[realKey] = lock
	}
	mol.lock.Unlock()
	lock.Lock()
}

func (mol *MemObjectMutex) Unlock(key string) {
	realKey := base64.StdEncoding.EncodeToString([]byte(mol.prefix + key))
	mol.lock.Lock()
	lock, ok := mol.lockMap[realKey]
	if ok {
		lock.Unlock()
	}
	mol.lock.Unlock()
}

//更新过期自动解锁时间 默认1分钟 检测时间为10秒检测一次
func (mol *MemObjectMutex) SetLockExpireTime(duration time.Duration) {
	mol.lock.Lock()

	if duration < 10*time.Second {
		mol.expireTime = 10 * time.Second
	} else {
		mol.expireTime = duration
	}

	if mol.expireTime > mol.gcTime {
		mol.gcTime = mol.expireTime + 10*time.Second
	}
	mol.lock.Unlock()
}

// 启动定时器去检查锁
// （遍历map的方式数据量大的情况下不太好，
// 需要改为记录时间排序，从时间最大的锁遍历，
// 遇到时间小于超时时间的退出循环）
// 默认30秒检测一次锁超时 10分钟内没有加锁的会删除
func (mol *MemObjectMutex) startGC() {
	timerFunc := func() {
		mol.lock.Lock()
		for k, v := range mol.lockMap {
			l := v.(*TimeOutMutex)
			if ok := l.CheckTimeOut(mol.gcTime); ok {
				delete(mol.lockMap, k)
				continue
			}
		}
		mol.lock.Unlock()
	}
	go func() {
		tick := time.NewTicker(5 * time.Second)
		for {
			select {
			case <-tick.C:
				timerFunc()
			}
		}
	}()
}

func NewMemObjectMutex(prefix string) ObjectMutex {
	ol := &MemObjectMutex{
		expireTime: 1 * time.Minute,
		gcTime:     10 * time.Minute,
		prefix:     prefix,
		lockMap:    make(map[interface{}]sync.Locker),
	}
	ol.startGC()
	return ol
}
