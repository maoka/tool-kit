package mutex

import (
	"encoding/base64"
	"sync"
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/maoka/tool-kit/utils"
)

type RedisMutexItem struct {
	key        string
	value      string
	lock       sync.Mutex
	expireTime time.Duration
	pool       *redis.Pool
	tries      int           // 尝试加锁次数
	delay      time.Duration // 检测间隔
	lockTime   time.Time     // 记录加锁时间
}

func (m *RedisMutexItem) Lock() {
	m.lock.Lock()
	defer m.lock.Unlock()

	value, _ := utils.GenerateRandomString(32)

	for i := 0; i < m.tries; i++ {
		if i != 0 {
			time.Sleep(m.delay)
		}
		if ok := m.acquire(value); ok {
			m.lockTime = time.Now()
			m.value = value
			return
		} else {
			// 本机检测是否超时
			if time.Now().Sub(m.lockTime) > m.expireTime+2*time.Millisecond {
				// TODO:警告 有问题
				break
			}
		}
	}
	if ok := m.refresh(value); ok {
		m.lockTime = time.Now()
		m.value = value
	}
}

func (m *RedisMutexItem) Unlock() {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.release()
	m.lockTime = time.Now().Add(-1 * (m.expireTime + 2*time.Millisecond))
}
func (m *RedisMutexItem) refresh(value string) bool {
	conn := m.pool.Get()
	defer conn.Close()
	reply, err := redis.String(conn.Do("SET", m.key, value, "PX", int(m.expireTime/time.Millisecond)))
	return err == nil && reply == "OK"
}

func (m *RedisMutexItem) acquire(value string) bool {
	conn := m.pool.Get()
	defer conn.Close()
	reply, err := redis.String(conn.Do("SET", m.key, value, "NX", "PX", int(m.expireTime/time.Millisecond)))
	return err == nil && reply == "OK"
}

func (m *RedisMutexItem) release() {
	conn := m.pool.Get()
	defer conn.Close()
	deleteScript.Do(conn, m.key, m.value)
	// ret, err := deleteScript.Do(conn, m.key, m.value)
}

var deleteScript = redis.NewScript(1, `
	if redis.call("GET", KEYS[1]) == ARGV[1] then
		return redis.call("DEL", KEYS[1])
	else
		return 0
	end
`)

type RedisObjectMutex struct {
	pool       *redis.Pool
	prefix     string
	expireTime time.Duration
	gcTime     time.Duration
	lockMap    map[interface{}]sync.Locker
	lock       sync.Mutex
	tries      int           // 尝试加锁次数
	delay      time.Duration // 检测间隔
}

func (rom *RedisObjectMutex) Lock(key string) {
	realKey := base64.StdEncoding.EncodeToString([]byte(rom.prefix + key))
	rom.lock.Lock()
	lock, ok := rom.lockMap[realKey]
	if !ok {
		lock = &RedisMutexItem{
			key:        realKey,
			expireTime: rom.expireTime,
			pool:       rom.pool,
			tries:      rom.tries,
			delay:      rom.delay,
			lockTime:   time.Now(),
		}
		rom.lockMap[realKey] = lock
	}
	rom.lock.Unlock()
	lock.Lock()
}

//对象解锁
func (rom *RedisObjectMutex) Unlock(key string) {
	realKey := base64.StdEncoding.EncodeToString([]byte(rom.prefix + key))
	rom.lock.Lock()
	lock, ok := rom.lockMap[realKey]
	if ok {
		lock.Unlock()
	}
	rom.lock.Unlock()

}

//更新过期自动解锁时间 默认1分钟 小于10秒则按10秒计算
func (rom *RedisObjectMutex) SetLockExpireTime(duration time.Duration) {
	rom.lock.Lock()
	defer rom.lock.Unlock()
	if duration < time.Second*10 {
		rom.expireTime = time.Second * 10
	} else {
		rom.expireTime = duration
	}
	if rom.expireTime > rom.gcTime {
		rom.gcTime = rom.expireTime + 10*time.Second
	}
}

// 释放锁（从map释放）
func (mol *RedisObjectMutex) startGC() {
	timerFunc := func() {
		mol.lock.Lock()
		for k, v := range mol.lockMap {
			l := v.(*RedisMutexItem)
			if time.Now().Sub(l.lockTime) > mol.gcTime+2*time.Millisecond {
				delete(mol.lockMap, k)
				continue
			}
		}
		mol.lock.Unlock()
	}
	go func() {
		tick := time.NewTicker(5 * time.Second)
		for {
			select {
			case <-tick.C:
				timerFunc()
			}
		}
	}()
}

func NewRedisObjectMutex(pool *redis.Pool, prefix string) ObjectMutex {
	rom := &RedisObjectMutex{
		pool:       pool,
		prefix:     prefix,
		expireTime: 300 * time.Second,
		gcTime:     10 * time.Minute,
		lockMap:    make(map[interface{}]sync.Locker),
		tries:      301,
		delay:      time.Millisecond * 100,
	}
	rom.startGC()
	return rom
}
