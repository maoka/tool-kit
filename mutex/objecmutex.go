package mutex

import "time"

type ObjectMutex interface {
	//对象加锁
	Lock(string)
	//对象解锁
	Unlock(string)
	//更新过期自动解锁时间 默认1分钟 小于10秒则按10秒计算
	SetLockExpireTime(duration time.Duration)
	//设置锁前缀 默认为空
	//SetPrefix(string)
}
