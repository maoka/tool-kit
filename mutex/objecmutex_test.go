package mutex

import (
	"gitlab.com/maoka/tool-kit/cache"
	"log"
	"sync"
	"testing"
	"time"
)

func testlockxixi(ol ObjectMutex, wg *sync.WaitGroup, str string) {
	ol.Lock("xixi")
	for i := 0; i < 10; i++ {
		log.Println(str, i)
		time.Sleep(time.Second * 1)
	}
	ol.Unlock("xixi")
	wg.Done()
	log.Println(str, " finished.")
}

func testlockhaha(ol ObjectMutex, wg *sync.WaitGroup, str string) {
	ol.Lock("haha")
	for i := 0; i < 10; i++ {
		log.Println(str, i)
		time.Sleep(time.Second * 1)
	}
	ol.Unlock("haha")
	wg.Done()
	log.Println(str, " finished.")
}

func lockExpireTest(ol ObjectMutex, wg *sync.WaitGroup) {
	ol.Lock("hehe")
	time.Sleep(time.Second * 90)
	ol.Unlock("hehe")
	wg.Done()
}

func TestNewMemObjectLock(t *testing.T) {
	var wg sync.WaitGroup

	//objectlock := NewMemObjectMutex("")
	cache.InitRedisPool(&cache.Redis{
		Host: "127.0.0.1:6397",
	})
	pool := cache.RedisPool()
	objectlock := NewRedisObjectMutex(pool, "test:lock:")
	//objectlock.Lock("xixi")
	//objectlock.Lock("haha")
	wg.Add(7)
	go testlockxixi(objectlock, &wg, "xixi1")
	//go testlockxixi(objectlock, &wg, "xixi2")
	//go testlockxixi(objectlock, &wg, "xixi3")
	//go testlockhaha(objectlock, &wg, "haha1")
	//go testlockhaha(objectlock, &wg, "haha2")
	go testlockhaha(objectlock, &wg, "haha3")
	go lockExpireTest(objectlock, &wg)
	wg.Wait()

}
