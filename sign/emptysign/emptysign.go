package emptysign

import "gitlab.com/maoka/tool-kit/sign"

type EmptySign struct{}

func (s *EmptySign) Sign(data []byte, key []byte) (string, error) {
	return "", nil
}
func (s *EmptySign) Verify(data []byte, key []byte, sign string) error {
	return nil
}
func init() {
	sign.RegisterSignObject("emptysign", &EmptySign{})
}
