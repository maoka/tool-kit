package keys

import (
	"fmt"
	"sync"
)

var keyList = make(map[string][]byte)
var keyListRWMutex sync.RWMutex

func RegisterKeys(name string, value []byte) error {
	if name == "" || value == nil {
		return fmt.Errorf("keys: parameter error")
	}
	keyListRWMutex.Lock()
	defer keyListRWMutex.Unlock()
	keyList[name] = value
	return nil
}

func GetKey(name string) []byte {
	if name == "" {
		return nil
	}
	keyListRWMutex.RLock()
	defer keyListRWMutex.RUnlock()
	if v, ok := keyList[name]; ok {
		return v
	}
	return nil
}
