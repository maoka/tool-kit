package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

type ApiMetrics struct {
	Name    string  //接口名
	Latency float64 //耗时
	Result  bool    //处理结果
}

var (
	metricsChan = make(chan ApiMetrics, 10)

	apiRequestDuration = prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Name: "api_request_duration",
		Help: "api request duration",
	}, []string{"endpoint"})

	apiRequestFailedCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "api_request_failed_count",
		Help: "api request failed count",
	},
		[]string{"endpoint"},
	)
	apiRequestSuccCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "api_request_succ_count",
		Help: "api request success count",
	},
		[]string{"endpoint"},
	)
)

func SendMetrics(metrics ApiMetrics) {
	metricsChan <- metrics
}

func StartMetrics() {
	prometheus.MustRegister(apiRequestDuration,
		apiRequestFailedCounter, apiRequestSuccCounter)
	metricsFunc := func(metrics ApiMetrics) {
		apiRequestDuration.WithLabelValues(metrics.Name).Observe(metrics.Latency)
		if metrics.Result {
			apiRequestSuccCounter.WithLabelValues(metrics.Name).Inc()
		} else {
			apiRequestFailedCounter.WithLabelValues(metrics.Name).Inc()
		}
	}

	go func() {
		for {
			select {
			case msg := <-metricsChan:
				metricsFunc(msg)
			}
		}
	}()
}
