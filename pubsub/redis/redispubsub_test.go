package redispubsub_test

import (
	"context"
	"log"
	"testing"
	"time"

	"gitlab.com/maoka/tool-kit/cache"
	redispubsub "gitlab.com/maoka/tool-kit/pubsub/redis"
)

func init() {

}

type TestRedisPubSubCallback struct{}

func (t *TestRedisPubSubCallback) OnStart() {
	log.Println("redis pubsub started!")
}
func (t *TestRedisPubSubCallback) OnMessage(channel string, data []byte) {
	log.Printf("recive from:%s data:%s", channel, string(data))
}
func (t *TestRedisPubSubCallback) OnClose(err error) {
	log.Println("redis pubsub closed err:", err)
}

func TestNewMKPubSubConn(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	cache.InitRedisPool(&cache.Redis{
		Host: "127.0.0.1:6379",
	})
	pubSub := redispubsub.NewMKPubSubConn(ctx, cache.RedisPool(), &TestRedisPubSubCallback{}, "haha1", "haha2")
	if pubSub == nil {
		t.Fatal()
	}
	time.Sleep(time.Second * 3)
	defer cancel()
}
