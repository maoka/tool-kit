package redispubsub

import (
	"container/list"
	"context"
	"time"

	"github.com/gomodule/redigo/redis"
)

const healthCheckPeriod = time.Second * 10

type MKPubSubCallback interface {
	OnStart()
	OnMessage(channel string, data []byte)
	OnClose(error)
}

type MKPubSubConn struct {
	psc      redis.PubSubConn
	channels list.List
	pool     *redis.Pool
	callFunc MKPubSubCallback
	done     chan error
}

func (mkpsc *MKPubSubConn) reconnect() error {
	mkpsc.psc.Close()
	return mkpsc.start()
}

func (mkpsc *MKPubSubConn) healthCheck() {
	if err := mkpsc.psc.Ping(""); err != nil {
		mkpsc.reconnect()
	}
}

func (mkpsc *MKPubSubConn) start() error {
	c := mkpsc.pool.Get()
	mkpsc.psc = redis.PubSubConn{Conn: c}
	if mkpsc.channels.Len() > 0 {
		for e := mkpsc.channels.Front(); e != nil; e = e.Next() {
			if err := mkpsc.psc.Subscribe(e.Value); err != nil {
				return err
			}
		}
	}
	if mkpsc.callFunc != nil {
		mkpsc.callFunc.OnStart()
	}
	return nil
}

func (mkpsc *MKPubSubConn) update() {
	for {
		switch n := mkpsc.psc.Receive().(type) {
		case error:
			mkpsc.done <- n
			return
		case redis.Message:
			if mkpsc.callFunc != nil {
				mkpsc.callFunc.OnMessage(n.Channel, n.Data)
			}
		case redis.Subscription:
			// switch n.Count {
			// case len(channels):
			// 	// Notify application when all channels are subscribed.
			// 	if err := onStart(); err != nil {
			// 		done <- err
			// 		return
			// 	}
			// case 0:
			// 	// Return from the goroutine when all channels are unsubscribed.
			// 	mkpsc.done <- nil
			// 	return
			// }
		}
	}
}

func (mkpsc *MKPubSubConn) Close() {
	mkpsc.psc.Close()
	if mkpsc.callFunc != nil {
		mkpsc.callFunc.OnClose(nil)
	}
	var channels []string
	for e := mkpsc.channels.Front(); e != nil; e = e.Next() {
		channels = append(channels, e.Value.(string))
	}
	mkpsc.psc.Unsubscribe(redis.Args{}.AddFlat(channels)...)
}

func (mkpsc *MKPubSubConn) Subscribe(channel ...string) error {
	for _, v := range channel {
		newChannel := true
		for e := mkpsc.channels.Front(); e != nil; e = e.Next() {
			if e.Value.(string) == v {
				newChannel = false
				break
			}
		}
		if newChannel {
			mkpsc.channels.PushBack(v)
			if err := mkpsc.psc.Subscribe(v); err != nil {
				return err
			}
		}
	}
	return nil
}

func (mkpsc *MKPubSubConn) UnSubscribe(channel ...string) error {
	for _, v := range channel {
		for e := mkpsc.channels.Front(); e != nil; {
			if e.Value.(string) == v {
				err := mkpsc.psc.Unsubscribe(v)
				if err != nil {
					return err
				}
				o := e
				e = e.Next()
				mkpsc.channels.Remove(o)
			}
		}
	}
	return nil
}
func NewMKPubSubConn(ctx context.Context, pool *redis.Pool, callFunc MKPubSubCallback, channels ...string) *MKPubSubConn {
	pubSubConn := &MKPubSubConn{
		pool:     pool,
		callFunc: callFunc,
		done:     make(chan error, 1),
	}
	if err := pubSubConn.start(); err != nil {
		return nil
	}
	go pubSubConn.update()
	if err := pubSubConn.Subscribe(channels...); err != nil {
		pubSubConn.Close()
		return nil
	}
	go func(ctx context.Context, pubSubConn *MKPubSubConn) {
		ticker := time.NewTicker(healthCheckPeriod)
		defer ticker.Stop()
		for {
			select {
			case err := <-pubSubConn.done:
				if pubSubConn.callFunc != nil {
					pubSubConn.callFunc.OnClose(err)
				}
			case <-ticker.C:
				pubSubConn.healthCheck()
			case <-ctx.Done():
				pubSubConn.Close()
				return
			}
		}
	}(ctx, pubSubConn)
	return pubSubConn
}

// redis订阅
// func ListenPubSubChannels(ctx context.Context, pool *redis.Pool,
// 	onStart func() error,
// 	onMessage func(channel string, data []byte) error,
// 	subChannel <-chan string,
// 	unSubChannel <-chan string,
// 	channels ...string) error {
// 	// A ping is set to the server with this period to test for the health of
// 	// the connection and server.
// 	const healthCheckPeriod = time.Minute

// 	c := pool.Get()
// 	defer c.Close()

// 	psc := redis.PubSubConn{Conn: c}
// 	if err := psc.Subscribe(redis.Args{}.AddFlat(channels)...); err != nil {
// 		return err
// 	}

// 	done := make(chan error, 1)
// 	// Start a goroutine to receive notifications from the server.
// 	go func() {
// 		for {
// 			switch n := psc.Receive().(type) {
// 			case error:
// 				done <- n
// 				return
// 			case redis.Message:
// 				if err := onMessage(n.Channel, n.Data); err != nil {
// 					done <- err
// 					return
// 				}
// 			case redis.Subscription:
// 				switch n.Count {
// 				case len(channels):
// 					// Notify application when all channels are subscribed.
// 					if err := onStart(); err != nil {
// 						done <- err
// 						return
// 					}
// 				case 0:
// 					// Return from the goroutine when all channels are unsubscribed.
// 					done <- nil
// 					return
// 				}
// 			}
// 		}
// 	}()

// 	ticker := time.NewTicker(healthCheckPeriod)
// 	defer ticker.Stop()
// loop:
// 	for {
// 		select {
// 		case <-ticker.C:
// 			// 保持连接
// 			if err := psc.Ping(""); err != nil {
// 				psc.Close()
// 				psc, _ = newPubSubChannels(pool, channels)
// 				// break loop
// 			}
// 		case <-ctx.Done():
// 			break loop
// 		case err := <-done:
// 			// 主程序结束
// 			return err
// 		case sub := <-subChannel:
// 			if err := psc.Subscribe(sub); err != nil {
// 				break loop
// 			}
// 		case unSub := <-unSubChannel:
// 			if err := psc.Unsubscribe(unSub); err != nil {
// 				break loop
// 			}

// 		}
// 	}
// 	// Signal the receiving goroutine to exit by unsubscribing from all channels.
// 	psc.Unsubscribe()
// 	return <-done
// }
