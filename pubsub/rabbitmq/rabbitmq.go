package rabbitmq

import (
	"github.com/streadway/amqp"
)

type RMQPubSubChannels struct {
	rmqConn     *amqp.Connection
	rmqChannel  *amqp.Channel
	channelName string
}

//只是发布消息第三个参数传nil
func NewRMQPubSubChannels(addr, name string,
	onMsg func(msg []byte)) (*RMQPubSubChannels, error) {
	var rmqChannelsInfo RMQPubSubChannels
	conn, err := amqp.Dial(addr)
	if err != nil {
		return nil, err
	}
	rmqChannelsInfo.rmqConn = conn

	channel, err := conn.Channel()
	if err != nil {
		return nil, err
	}
	rmqChannelsInfo.rmqChannel = channel
	rmqChannelsInfo.channelName = name

	err = rmqChannelsInfo.rmqChannel.ExchangeDeclare(
		name,     // name
		"fanout", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		return nil, err
	}

	if onMsg != nil {
		q, err := rmqChannelsInfo.rmqChannel.QueueDeclare(
			"",    // name
			false, // durable
			false, // delete when usused
			true,  // exclusive
			false, // no-wait
			nil,   // arguments
		)
		if err != nil {
			return nil, err
		}
		// log.Println("rabbit mq queue name:", q.Name)
		err = rmqChannelsInfo.rmqChannel.QueueBind(
			q.Name,                      // queue name
			"",                          // routing key
			rmqChannelsInfo.channelName, // exchange
			false,
			nil)
		if err != nil {
			return nil, err
		}

		msgs, err := rmqChannelsInfo.rmqChannel.Consume(
			q.Name, // queue
			"",     // consumer
			true,   // auto-ack
			false,  // exclusive
			false,  // no-local
			false,  // no-wait
			nil,    // args
		)

		go func() {
			for d := range msgs {
				onMsg(d.Body)
			}
		}()
	}
	return &rmqChannelsInfo, nil
}

// 发布消息
func (rmq *RMQPubSubChannels) Publish(msg []byte) error {
	if msg == nil {
		return nil
	}
	err := rmq.rmqChannel.Publish(
		rmq.channelName,
		"",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        msg,
		})
	return err
}

// 关闭连接
func (rmq *RMQPubSubChannels) Close() {
	rmq.rmqChannel.Close()
	rmq.rmqConn.Close()
}
