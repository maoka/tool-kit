package rabbitmq

import (
	"log"
	"testing"
	"time"
)

func testOnMessage(msg []byte) {
	log.Printf(" %s ", string(msg))
}
func TestNewRMQPubSubChannels(t *testing.T) {
	mq, err := NewRMQPubSubChannels(
		"amqp://guest:guest@10.7.6.227:5672/",
		"logs",
		testOnMessage,
	)
	if err != nil {
		t.Fatal(err)
	}

	err = mq.Publish([]byte("xixixihahaha123"))
	if err != nil {
		t.Fatal(err)
	}
	err = mq.Publish([]byte("xueyejus"))
	if err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Second * 10000)
}
