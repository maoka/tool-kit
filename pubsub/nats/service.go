package nats

import (
	"errors"
	"reflect"
	"strings"
	"unicode"
	"unicode/utf8"

	"gitlab.com/maoka/tool-kit/peer"
)

type Handler struct {
	Receiver reflect.Value  // 值
	Method   reflect.Method // 方法
	Type     reflect.Type   // 类型
	IsRawArg bool           // 数据是否需要序列化
}

type Service struct {
	Name     string              // 服务名
	Type     reflect.Type        // 服务类型
	Receiver reflect.Value       // 服务值
	Handlers map[string]*Handler // 注册的方法列表
}

var (
	typeOfError   = reflect.TypeOf((*error)(nil)).Elem()
	typeOfBytes   = reflect.TypeOf(([]byte)(nil))
	typeOfSession = reflect.TypeOf(peer.NewSession(nil))
)

func isExported(name string) bool {
	w, _ := utf8.DecodeRuneInString(name)
	return unicode.IsUpper(w)
}

// 方法检测
func isHandlerMethod(method reflect.Method, resp bool) bool {
	mt := method.Type
	if method.PkgPath != "" {
		return false
	}
	if resp {
		if mt.NumIn() != 2 {
			return false
		}
		if mt.NumOut() != 2 {
			return false
		}
		if (mt.In(1).Kind() != reflect.Ptr && mt.In(1) != typeOfBytes) || mt.Out(1) != typeOfError || mt.Out(0).Kind() != reflect.Ptr {
			return false
		}
	} else {
		if mt.NumIn() != 2 {
			return false
		}
		if mt.NumOut() != 1 {
			return false
		}
		if (mt.In(1).Kind() != reflect.Ptr && mt.In(1) != typeOfBytes) || mt.Out(0) != typeOfError {
			return false
		}
	}
	return true
}

func newService(comp interface{}) *Service {
	s := &Service{
		Type:     reflect.TypeOf(comp),
		Receiver: reflect.ValueOf(comp),
	}
	s.Name = strings.ToLower(reflect.Indirect(s.Receiver).Type().Name())
	return s
}

// 遍历取出满足条件的函数
func (s *Service) suitableHandlerMethods(typ reflect.Type, resp bool) map[string]*Handler {
	methods := make(map[string]*Handler)
	for m := 0; m < typ.NumMethod(); m++ {
		method := typ.Method(m)
		mt := method.Type
		mn := method.Name
		if isHandlerMethod(method, resp) {
			raw := false
			if mt.In(1) == typeOfBytes {
				raw = true
			}
			mn = strings.ToLower(mn)
			methods[mn] = &Handler{Method: method, Type: mt.In(1), IsRawArg: raw}
		}
	}
	return methods
}

// 取结构体中的函数
func (s *Service) ExtractHandler(resp bool) error {
	typeName := reflect.Indirect(s.Receiver).Type().Name()
	if typeName == "" {
		return errors.New("no service name for type " + s.Type.String())
	}
	if !isExported(typeName) {
		return errors.New("type " + typeName + " is not exported")
	}
	s.Handlers = s.suitableHandlerMethods(s.Type, resp)

	if len(s.Handlers) == 0 {
		str := "service: "
		method := s.suitableHandlerMethods(reflect.PtrTo(s.Type), resp)
		if len(method) != 0 {
			str = "type " + s.Name + " has no exported methods of suitable type (hint: pass a pointer to value of that type)"
		} else {
			str = "type " + s.Name + " has no exported methods of suitable type"
		}
		return errors.New(str)
	}
	for i := range s.Handlers {
		s.Handlers[i].Receiver = s.Receiver
	}
	return nil
}
