package nats

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"gitlab.com/maoka/tool-kit/codec"
	"gitlab.com/maoka/tool-kit/utils"
	"reflect"
	"strings"
	"time"
)

type NATSCli struct {
	conn                 *nats.Conn
	c                    codec.Codec
	rpcHandlerList       map[string]map[string]*Service
	subscribeHandlerList map[string]map[string]*Service
}

//调用注册的函数
func callHandlerFunc(foo reflect.Method, args []reflect.Value) (retValue interface{}, retErr error) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(fmt.Sprintf("callHandlerFunc: %v", err))
			fmt.Println(utils.CallStack())
			retValue = nil
			retErr = fmt.Errorf("callHandlerFunc: call method pkg:%s method:%s err:%v", foo.PkgPath, foo.Name, err)
		}
	}()
	if ret := foo.Func.Call(args); len(ret) > 0 {
		var err error = nil
		if len(ret) == 2 {
			if r1 := ret[1].Interface(); r1 != nil {
				err = r1.(error)
			}
			return ret[0].Interface(), err
		} else if len(ret) == 1 {
			if r0 := ret[0].Interface(); r0 != nil {
				err = r0.(error)
			}
			return nil, err
		}
	}
	return nil, fmt.Errorf("callHandlerFunc: call method pkg:%s method:%s", foo.PkgPath, foo.Name)
}

func NewNATSCli(addr, codecType string) (*NATSCli, error) {
	conn, err := nats.Connect(addr, nats.Timeout(3*time.Second))
	if err != nil {
		return nil, fmt.Errorf("nats connect failed. err:%v", err)
	}
	cli := &NATSCli{
		conn:                 conn,
		rpcHandlerList:       make(map[string]map[string]*Service),
		subscribeHandlerList: make(map[string]map[string]*Service),
	}
	cliCodec := codec.GetCodec(codecType)
	if cliCodec == nil {
		return nil, fmt.Errorf("codec:%s not found.", codecType)
	}
	cli.c = cliCodec
	return cli, nil
}

func (ns *NATSCli) Call(subj, api string, req, ack interface{}) error {
	if reflect.TypeOf(req).Kind() != reflect.Ptr {
		return fmt.Errorf("nats: rpc call parameter cannot be a non-pointer req:%T", req)
	}
	if ack != nil {
		if reflect.TypeOf(ack).Kind() != reflect.Ptr {
			return fmt.Errorf("nats: rpc call parameter cannot be a non-pointer ack:%T", ack)
		}
		if codec.GetMessage(api) == nil {
			codec.RegisterMessage(api, ack)
		}
	}
	rb, err := ns.c.Marshal(api, req, nil)
	if err != nil {
		return err
	}
	msg, err := ns.conn.Request(subj, rb, time.Second*5)
	if err != nil {
		return err
	}
	if ack != nil {
		_, pack, err := ns.c.Unmarshal(msg.Data)
		if err != nil {
			return err
		}
		if pack.Err != nil {
			return pack.Err
		}
		if pack.DataPtr != nil {
			reflect.ValueOf(ack).Elem().Set(reflect.ValueOf(pack.DataPtr).Elem())
		}
	}
	return nil
}

func (ns *NATSCli) RPCHander(subj, queue string, svrs ...interface{}) error {
	if subj == "" {
		return fmt.Errorf("invalid subj:%s", subj)
	}
	serviceList, ok := ns.rpcHandlerList[subj]
	if !ok {
		serviceList = make(map[string]*Service)
		ns.rpcHandlerList[subj] = serviceList
	}
	for _, v := range svrs {
		s := newService(v)
		if _, ok := serviceList[s.Name]; ok {
			return fmt.Errorf("service: service already defined: %s", s.Name)
		}
		if err := s.ExtractHandler(true); err != nil {
			return fmt.Errorf("service: extract handler function failed: %v", err)
		}
		serviceList[s.Name] = s
		for name, handler := range s.Handlers {
			router := fmt.Sprintf("%s.%s", s.Name, name)
			codec.RegisterMessage(router, handler.Type)
		}
	}
	if !ok {
		_, err := ns.conn.QueueSubscribe(subj, queue, func(msg *nats.Msg) {
			_, msgPack, err := ns.c.Unmarshal(msg.Data)
			if err != nil {
				fmt.Printf("onreceive: %v", err)
				return
			}
			router, ok := msgPack.Router.(string)
			if !ok {
				fmt.Printf("onreceive: invalid router:%v", msgPack.Router)
				return
			}
			routerArr := strings.Split(router, ".")
			if len(routerArr) != 2 {
				fmt.Printf("onreceive: invalid router:%s", msgPack.Router)
				return
			}
			s, ok := serviceList[routerArr[0]]
			if !ok {
				fmt.Printf("onreceive: function not registed router:%s err:%v", msgPack.Router, err)
				return
			}
			h, ok := s.Handlers[routerArr[1]]
			if !ok {
				fmt.Printf("onreceive: function not registed router:%s err:%v", msgPack.Router, err)
				return
			}
			var args = []reflect.Value{h.Receiver, reflect.ValueOf(msgPack.DataPtr)}
			ack, err := callHandlerFunc(h.Method, args)
			if err != nil {
				fmt.Println(fmt.Sprintf("onreceive: call func failed. router:%s err:%v", msgPack.Router, err))
				rb, err := ns.c.Marshal(router, nil, err)
				if err != nil {
					fmt.Println(err)
					return
				}
				msg.Respond(rb)
			} else {
				if ack != nil && !reflect.ValueOf(ack).IsNil() {
					rb, err := ns.c.Marshal(router, ack, nil)
					if err != nil {
						fmt.Println(err)
						return
					}
					msg.Respond(rb)
				}
			}
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func (ns *NATSCli) SubscribeHandler(subj string, svrs ...interface{}) error {
	if subj == "" {
		return fmt.Errorf("invalid subj:%s", subj)
	}
	serviceList, ok := ns.subscribeHandlerList[subj]
	if !ok {
		serviceList = make(map[string]*Service)
		ns.subscribeHandlerList[subj] = serviceList
	}
	for _, v := range svrs {
		s := newService(v)
		if _, ok := serviceList[s.Name]; ok {
			return fmt.Errorf("service: service already defined: %s", s.Name)
		}
		if err := s.ExtractHandler(false); err != nil {
			return fmt.Errorf("service: extract handler function failed: %v", err)
		}
		serviceList[s.Name] = s
		for name, handler := range s.Handlers {
			router := fmt.Sprintf("%s.%s", s.Name, name)
			codec.RegisterMessage(router, handler.Type)
		}
	}
	if !ok {
		_, err := ns.conn.Subscribe(subj, func(msg *nats.Msg) {
			_, msgPack, err := ns.c.Unmarshal(msg.Data)
			if err != nil {
				fmt.Printf("onreceive: %v", err)
				return
			}
			router, ok := msgPack.Router.(string)
			if !ok {
				fmt.Printf("onreceive: invalid router:%v", msgPack.Router)
				return
			}
			routerArr := strings.Split(router, ".")
			if len(routerArr) != 2 {
				fmt.Printf("onreceive: invalid router:%s", msgPack.Router)
				return
			}
			s, ok := serviceList[routerArr[0]]
			if !ok {
				fmt.Printf("onreceive: function not registed router:%s err:%v", msgPack.Router, err)
				return
			}
			h, ok := s.Handlers[routerArr[1]]
			if !ok {
				fmt.Printf("onreceive: function not registed router:%s err:%v", msgPack.Router, err)
				return
			}
			var args = []reflect.Value{h.Receiver, reflect.ValueOf(msgPack.DataPtr)}
			_, err = callHandlerFunc(h.Method, args)
			if err != nil {
				fmt.Printf("onreceive: call func failed. router:%s err:%v", msgPack.Router, err)
				return
			}
		})
		if err != nil {
			return err
		}
	}
	return nil
}

//消息广播
func (ns *NATSCli) Publish(subj, api string, req interface{}) error {
	rb, err := ns.c.Marshal(api, req, nil)
	if err != nil {
		return err
	}
	ns.conn.Publish(subj, rb)
	ns.conn.Flush()
	return nil
}
