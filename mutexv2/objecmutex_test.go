package mutex

import (
	"gitlab.com/maoka/tool-kit/cache"
	"log"
	"sync"
	"testing"
	"time"
)

func testlockxixi(ol sync.Locker, wg *sync.WaitGroup, str string) {
	ol.Lock()
	for i := 0; i < 10; i++ {
		log.Println(str, i)
		time.Sleep(time.Second * 1)
	}
	ol.Unlock()
	wg.Done()
	log.Println(str, " finished.")
}

func testlockhaha(ol sync.Locker, wg *sync.WaitGroup, str string) {
	ol.Lock()
	for i := 0; i < 10; i++ {
		log.Println(str, i)
		time.Sleep(time.Second * 1)
	}
	ol.Unlock()
	wg.Done()
	log.Println(str, " finished.")
}

func lockExpireTest(ol sync.Locker, wg *sync.WaitGroup) {
	ol.Lock()
	time.Sleep(time.Second * 90)
	ol.Unlock()
	wg.Done()
}

func TestNewMemObjectLock(t *testing.T) {
	var wg sync.WaitGroup

	//objectlock := NewMemObjectMutex("")
	cache.InitRedisPool(&cache.Redis{
		Host: "127.0.0.1",
	})
	pool := cache.RedisPool()
	objectlock := NewRedisObjectMutex(pool, "test:lock:")
	//objectlock.Lock("xixi")
	//objectlock.Lock("haha")
	wg.Add(6)
	go testlockxixi(objectlock.NewMutex("xixi"), &wg, "xixi1")
	go testlockxixi(objectlock.NewMutex("xixi"), &wg, "xixi2")
	go testlockxixi(objectlock.NewMutex("xixi"), &wg, "xixi3")
	go testlockhaha(objectlock.NewMutex("haha"), &wg, "haha1")
	go testlockhaha(objectlock.NewMutex("haha"), &wg, "haha2")
	go testlockhaha(objectlock.NewMutex("haha"), &wg, "haha3")
	//go lockExpireTest(objectlock.NewMutex("hehe"), &wg)
	wg.Wait()

}
