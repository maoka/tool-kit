package mutex

import (
	"sync"
	"time"
)

type ObjectMutex interface {
	//得到一个对象锁
	NewMutex(string) sync.Locker
	//更新过期自动解锁时间 默认1分钟 小于10秒则按10秒计算
	SetLockExpireTime(time.Duration)
}
