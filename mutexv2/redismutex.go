package mutex

import (
	"sync"
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/maoka/tool-kit/utils"
)

type RedisMutexItem struct {
	key        string
	value      string
	lock       sync.Mutex
	expireTime time.Duration
	pool       *redis.Pool
	tries      int           // 尝试加锁次数
	delay      time.Duration // 检测间隔
}

func (m *RedisMutexItem) Lock() {
	m.lock.Lock()
	defer m.lock.Unlock()

	value, _ := utils.GenerateRandomString(32)

	start := time.Now()
	for i := 0; i < m.tries; i++ {
		if i != 0 {
			time.Sleep(m.delay)
		}
		if ok := m.acquire(value); ok {
			m.value = value
			return
		}
		until := time.Now().Add(m.expireTime - time.Now().Sub(start) + 2*time.Millisecond)
		if time.Now().After(until) {
			break
		}
	}
	if ok := m.refresh(value); ok {
		m.value = value
		return
	}
}

func (m *RedisMutexItem) Unlock() {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.release()
}
func (m *RedisMutexItem) refresh(value string) bool {
	conn := m.pool.Get()
	defer conn.Close()
	reply, err := redis.String(conn.Do("SET", m.key, value, "PX", int(m.expireTime/time.Millisecond)))
	return err == nil && reply == "OK"
}

func (m *RedisMutexItem) acquire(value string) bool {
	conn := m.pool.Get()
	defer conn.Close()
	reply, err := redis.String(conn.Do("SET", m.key, value, "NX", "PX", int(m.expireTime/time.Millisecond)))
	return err == nil && reply == "OK"
}

func (m *RedisMutexItem) release() {
	conn := m.pool.Get()
	defer conn.Close()
	// ret, err := deleteScript.Do(conn, m.key, m.value)
	// log.Println("DEL ", []string{m.key}, m.value, ret, err)
	deleteScript.Do(conn, m.key, m.value)
}

var deleteScript = redis.NewScript(1, `
	if redis.call("GET", KEYS[1]) == ARGV[1] then
		return redis.call("DEL", KEYS[1])
	else
		return 0
	end
`)

type RedisObjectMutex struct {
	pool       *redis.Pool
	prefix     string
	expireTime time.Duration
	lock       sync.Mutex
}

func (rom *RedisObjectMutex) NewMutex(key string) sync.Locker {
	lock := &RedisMutexItem{
		key:        rom.prefix + key,
		expireTime: rom.expireTime,
		pool:       rom.pool,
		tries:      301,
		delay:      100 * time.Millisecond,
	}
	return lock
}

//更新过期自动解锁时间 默认1分钟 小于10秒则按10秒计算
func (rom *RedisObjectMutex) SetLockExpireTime(duration time.Duration) {
	rom.lock.Lock()
	defer rom.lock.Unlock()
	if duration < time.Second*10 {
		rom.expireTime = time.Second * 10
	} else {
		rom.expireTime = duration
	}
}

func NewRedisObjectMutex(pool *redis.Pool, prefix string) ObjectMutex {
	rom := &RedisObjectMutex{
		pool:       pool,
		prefix:     prefix,
		expireTime: 300 * time.Second,
	}
	return rom
}
